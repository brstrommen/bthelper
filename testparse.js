'use strict';

const _ = require('lodash');
const fs = require('fs');
const parser = require('xml2js').parseString;


mapAllMechs().then(mechs => {
  console.log(mechs.length);
  writeToFile(mechs);
}).catch(err => {
  console.log(err);
});


function writeToFile(mechs) {
  //mechs = _.filter(mechs, mech => mech.tech === 'Inner Sphere');
  let json = JSON.stringify(mechs, null, 3);
  fs.writeFile('./all-mechs.json', json);
}

function mapAllMechs() {
  return new Promise((resolve, reject) => {
    fs.readdir('./src/assets/variants', (err, result) => {
      if (err) { console.log(err); return; }

      Promise.all(
        result.map(file => parseFile(`./src/assets/variants/${file}`))
      ).then(res => {
        res = _.filter(res, mech => mech.mech_type[0] === 'BattleMech');

        let mechs = [ ];

        _.forEach(res, mech => {
          if (mech.loadout) {
            mapMechOmni(mech).forEach(omni => mechs.push(omni));
          } else {
            mechs.push(mapMechVariant(mech));
          }
        });

        resolve(mechs);
      }).catch(err => reject(err));
    });
  });
}

function mapMechVariant(mech) {
  console.log(`Mapping Mech Variant: ${mech['$'].name} ${mech['$'].model}`);
  let loadout = mech.baseloadout[0];
  let newMech = {
    chassis: mech['$'].name,
    variant: `${mech['$'].model}`,
    tons: parseInt(mech['$'].tons),
    BV: 0,
    //era: mech.era,
    //productionera: mech.productionera,
    tech: mech.techbase[0]['_'], //'Inner Sphere', 'Clan'
    //year: mech.year,
    weapons: [],
    heatsinks: parseInt(loadout.heatsinks[0].type[0] === 'Double Heat Sink' ? loadout.heatsinks[0]['$'].number * 2 : loadout.heatsinks[0]['$'].number),
    description: loadout.info[0],
    fullname: `${mech['$'].name} ${mech['$'].model}`,
    //XML: mech
  };

  _.forEach(
    _.filter(loadout.equipment, item =>
      ['energy', 'ballistic', 'missile', 'equipment'].indexOf(item.type[0]) !== -1
    ), item =>
    newMech.weapons.push({
      weapon: item.name[0]['_'],
      location: _.get(item, 'location', [{ _: 'Split Location' }])[0]['_']
    })
  );

  try { newMech.BV = parseInt(mech.battle_value[0]); } catch (err) { }

  return newMech;
}

function mapMechOmni(mech) {
  //console.log(`Mapping OmniMech: ${mech['$'].name}`);

  let allVariants = [];
  _.forEach(mech.loadout, variant => {
    console.log(`Mapping ${mech['$'].name} Variant: ${variant['$'].name}`);

    let equipment = variant.equipment;
    let omni = mapMechVariant(mech);
    omni.description = variant.info[0];
    omni.variant = variant['$'].name;
    omni.fullname = `${omni.chassis} ${omni.variant}`;

    _.forEach(
      _.filter(equipment, item =>
        ['energy', 'ballistic', 'missile', 'equipment'].indexOf(item.type[0]) !== -1
      ), item =>
      omni.weapons.push({
        weapon: item.name[0]['_'],
        location: _.get(item, 'location', [{ _: 'Split Location' }])[0]['_']
      })
    );

    allVariants.push(omni);
  });

  return allVariants;
}

function parseFile(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, (err, data) => {
      if (err) reject(err);

      parser(data, (err, result) => {
        if (err) reject(err);
        delete result.mech.fluff;
        resolve(result.mech);
      });
    });
  });
}

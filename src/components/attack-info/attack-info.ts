import {
  Component,
  Input
} from '@angular/core';

import { Weapon } from '../../classes';


@Component({
  selector: 'bth-attack-info',
  templateUrl: 'attack-info.html'
})
export class AttackInfoComponent {
  @Input() weapon: Weapon;
  public showInfo: boolean = false;
}

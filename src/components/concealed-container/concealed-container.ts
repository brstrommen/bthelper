import { Component } from '@angular/core';
import { AlertController, ModalController } from 'ionic-angular';
import { take } from 'rxjs/operators';

import { LanceService, TurnService } from '../../providers';
import { HideUnitPage } from '../../pages';


@Component({
  selector: 'concealed-container',
  templateUrl: 'concealed-container.html'
})
export class ConcealedContainerComponent {
  public collapsed: string = 'default';

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public lanceSvc: LanceService,
    public turnSvc: TurnService
  ) { }

  public concealUnit() {
    this.modalCtrl.create(HideUnitPage).present();
  }

  public revealUnit(mech) {
    this.turnSvc.turn$.pipe(take(1)).subscribe(currentTurn => {
      this.alertCtrl.create({
        title: mech.getFullName(),
        message: mech.location,
        buttons: [{
          text: 'Close',
          role: 'cancel'
        }, {
          text: 'Reveal',
          cssClass: 'danger',
          handler: () => {
            mech.location = `Turn ${currentTurn} in ${mech.location}`;
            mech.hidden = false;
            this.lanceSvc.saveMechs(true);
          }
        }]
      }).present();
    });
  }

  public minMechs() {
    switch(this.collapsed) {
      case 'collapsed': { this.collapsed = 'hidden'; break; }
      case 'hidden': { this.collapsed = 'default'; break; }
      default: { this.collapsed = 'collapsed'; break; }
    }
  }

}

import { Component } from '@angular/core';
import { AlertController, ToastController, ModalController } from 'ionic-angular';
import { take } from 'rxjs/operators';

import { ArtyService, TurnService } from '../../providers';
import { AddArtyPage } from '../../pages';


@Component({
  selector: 'arty-container',
  templateUrl: 'arty-container.html'
})
export class ArtyContainerComponent {
  public collapsed: string = 'default';

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public artySvc: ArtyService,
    public turnSvc: TurnService
  ) {

  }

  public promptArtyStrike() {
    let modal = this.modalCtrl.create(AddArtyPage);
    modal.onDidDismiss(data => {
      if (data) {
        this.toastCtrl.create({
          duration: 2250,
          message: 'Artillery Strike launched.',
          position: 'middle'
        }).present();
      }
    });

    modal.present();
  }

  public revealArty(strike) {
    this.turnSvc.turn$.pipe(take(1)).subscribe(currentTurn => {
      if (strike.turn != currentTurn) {
        this.toastCtrl.create({
          duration: 2250,
          message: 'This Artillery Strike is still in-flight.',
          position: 'middle'
        }).present();

        return;
      }

      this.alertCtrl.create({
        title: `${strike.type} Artillery Strike`,
        message: `Firing on ${strike.mapsheet} @ ${strike.hex} on Turn ${strike.turn}`,
        buttons: [{
          text: 'Close',
          role: 'cancel'
        }, {
          text: 'Reveal',
          cssClass: 'danger',
          handler: () => {
            this.artySvc.revealArty(strike);
          }
        }]
      }).present();
    });
  }

  public minStrikes() {
    switch(this.collapsed) {
      case 'collapsed': { this.collapsed = 'hidden'; break; }
      case 'hidden': { this.collapsed = 'default'; break; }
      default: { this.collapsed = 'collapsed'; break; }
    }
  }

}

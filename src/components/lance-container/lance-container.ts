import {
  Component,
  Input
} from '@angular/core';
import { ModalController, ToastController, AlertController } from 'ionic-angular';

import { forEach } from 'lodash';

import { AddPage } from '../../pages';
import { LanceService } from '../../providers';


@Component({
  selector: 'lance-container',
  templateUrl: 'lance-container.html'
})
export class LanceContainerComponent {
  @Input() self: boolean = true;
  @Input() units: any[] = [];
  public showMechs: boolean = true;

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public lanceSvc: LanceService
  ) {
    //
  }

  public editMech(unit) {
    this.alertCtrl.create({
      title: 'Edit Mech',
      inputs: [{
        name: 'piloting',
        type: 'number',
        placeholder: 'Piloting'
      }, {
        name: 'gunnery',
        type: 'number',
        placeholder: 'Gunnery'
      }, {
        name: 'description',
        placeholder: 'Description'
      }],
      buttons: [{
        text: 'Cancel', role: 'cancel'
      }, {
        text: 'Save', handler: (data) => {
          forEach(data, (value, key) => unit[key] = value || unit[key]);
          this.lanceSvc.saveMechs(this.self);
        }
      }]
    }).present();
  }

  public addUnit() {
    let modal = this.modalCtrl.create(AddPage);
    modal.onDidDismiss(data => {
      if(data) {
        this.showMechs = true;

        if (this.self) {
          this.lanceSvc.addMyUnit(data);

          let warn: boolean = false;
          forEach(data.weapons, weapon => {
            if(weapon.name.indexOf('Unsupported') !== -1) {
              warn = true;
            }
          });

          if (warn) {
            this.toastCtrl.create({
              position: 'middle',
              duration: 2750,
              message: `Warning: ${data.chassis} ${data.model} has unsupported weapon types.`
            }).present();
          }

        } else {
          this.lanceSvc.addOpForUnit(data);
        }
      }
    });

    modal.present();
  }

  public deleteUnit(unit) {
    if (this.self) {
      this.lanceSvc.deleteMyUnit(unit);
    } else {
      this.lanceSvc.deleteOpForUnit(unit);
    }
  }

}

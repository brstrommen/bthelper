import {
  Component,
  Input,
  OnInit,
  OnChanges
} from '@angular/core';
import { AlertController } from 'ionic-angular';

import { Unit, Weapon } from '../../classes';
import { map } from 'lodash';

@Component({
  selector: 'bth-attack',
  templateUrl: 'declare-attack.html'
})
export class AttackComponent implements OnInit, OnChanges {
  @Input('attacker') attacker: Unit;
  @Input('target') target: Unit;
  @Input('weapon') weapon: Weapon;
  @Input('secondary') secondary: boolean = false;

  @Input('gunnery') gunnery: number;
  @Input('moveType') moveType: number;
  @Input('moveMod') moveMod: number;
  @Input('other') other: number;
  @Input('range') range: number;

  public toHit: number;
  public checked: boolean;

  constructor(public alertCtrl: AlertController) { }

  ngOnInit() {
    this.checked = this.weapon.target === this.target;
  }

  ngOnChanges() {
    this.checked = this.weapon.target === this.target;

    this.toHit = this.weapon.calcToHit(
      this.gunnery,
      this.moveType,
      this.moveMod,
      this.other + (this.secondary ? 1 : 0),
      this.range
    );

    if (this.checked) {
      this.weapon.toHit = this.toHit;
    }

    if (this.toHit === NaN) {
      this.checked = false;
      this.weapon.target = null;
    }
  }

  private checkWeaponEffects() {
    this.weapon.target = this.checked ? this.target : null;

    if (this.checked) {
      this.weapon.toHit = this.toHit;
    }

    this.attacker.calculateTurnHeat();
  }

  public checkWeapon() {
    if (this.weapon['firingModes'] !== undefined) {
      if (this.checked) {
        this.checked = false;
        this.weapon['setFiringMode'](this.weapon['firingModes'[1]]);
        this.checkWeaponEffects();
        return;
      }

      this.alertCtrl.create({
        title: 'Select Firing Mode',
        inputs: map(this.weapon['firingModes'], mode => ({
          label: mode,
          type: 'radio',
          value: mode
        })),
        buttons: [{
          text: 'Cancel', handler: () => {
            this.checked = false;
            this.checkWeaponEffects();
          }
        }, {
          text: 'Select', handler: (data) => {
            this.checked = true;
            this.weapon['setFiringMode'](data);
            this.checkWeaponEffects();
          }
        }]
      }).present();
      return;
    }

    this.checked = !this.checked;
    this.checkWeaponEffects();
  }

}

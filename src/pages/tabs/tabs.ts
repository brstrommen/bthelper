import { Component } from '@angular/core';

import { HomePage } from '../';
import { CombatPage } from '../';
import { ClassifiedPage } from '../';
import { UtilitiesPage } from '../'


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = CombatPage;
  tab3Root = ClassifiedPage;
  tab4Root = UtilitiesPage;

  constructor() { }

}

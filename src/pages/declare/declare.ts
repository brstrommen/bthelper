import { Component } from '@angular/core';
import { AlertController, ViewController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';

import { difference, filter, find, forEach, last, map } from 'lodash';

import { LanceService } from '../../providers';
import { Target, Unit } from '../../classes';


@Component({
  selector: 'page-declare',
  templateUrl: 'declare.html'
})
export class DeclarePage {
  public attacker: Unit;

  constructor(
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public lanceSvc: LanceService
  ) {
    this.attacker = this.navParams.get('unit');
  }

  ngOnInit() {
    if (this.lanceSvc.opFor.length === 1) {
      this.attacker.targets[0].unit = this.lanceSvc.opFor[0];
    }
  }

  public setMoveType(moveType) {
    this.attacker.moveType = parseInt(moveType);
    this.attacker.calculateTurnHeat();
  }

  public setMoveMod(mod, target) {
    target.unit.moveMod = parseInt(mod);
  }

  public setRange(range, target) {
    target.range = parseInt(range);
  }

  public setCover(cover, target) {
    target.cover = parseInt(cover);
  }

  public addTarget() {
    this.attacker.targets.push(new Target);

    if (this.lanceSvc.opFor.length === this.attacker.targets.length) {
      last(this.attacker.targets).unit =
        difference(this.lanceSvc.opFor,
          map(this.attacker.targets, target => target.unit))[0]
    }
  }

  public jump(jump) {
    this.attacker.jumping = parseInt(jump) || 0;
    this.attacker.calculateTurnHeat();
  }

  public checkUnit(unit) {
    return find(this.attacker.targets, target => target.unit === unit);
  }

  public checkTarget(newUnit, oldTarget) {
    if (oldTarget.unit) {
      forEach(
        filter(this.attacker.weapons, weapon => weapon.target == oldTarget.unit),
        weapon => weapon.target = null
      );
    }

    oldTarget.unit = newUnit;
  }

}

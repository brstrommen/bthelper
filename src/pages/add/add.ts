import { Component, ViewChild } from '@angular/core';
import { ViewController, LoadingController } from 'ionic-angular';

import { filter } from 'lodash';

import { SSWService } from '../../providers';


@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {
  @ViewChild('searchInput') searchInput;

  public search: string = "";
  public mechs: any[];
  public index: number = 0;

  constructor(
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    public sswSvc: SSWService
  ) {
    //
  }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      spinner: 'dots',
      content: 'Loading Mech data...',
      duration: 500
    });

    loading.onDidDismiss(() =>
      setTimeout(() => this.searchInput.setFocus(), 150)
    );

    loading.present();
  }

  ionViewDidEnter() {
    this.mechs = this.sswSvc.mechs;
    this.index = 15;
  }

  public onInput() {
    this.mechs = filter(this.sswSvc.mechs,
      mech => mech.getFullName().toUpperCase()
        .indexOf(this.search.toUpperCase()) !== -1
    );
    this.index = 15;
  }

  public onCancel() {
    this.search = "";
    this.index = 15;
  }

  public doInfinite(scroll) {
    this.index += 15;
    setTimeout(() => scroll.complete(), 350);
  }

  public addMech(mech) {
    this.viewCtrl.dismiss(
      this.sswSvc.getMechCopy(`${mech.chassis} ${mech.model}`)
    );
  }

}

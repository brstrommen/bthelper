import { Component } from '@angular/core';

import { LanceService } from '../../providers';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public lanceSvc: LanceService
  ) {
    //
  }

}

import { Component } from '@angular/core';


@Component({
  selector: 'page-deadzone',
  templateUrl: 'deadzone.html'
})
export class DeadzonePage {
  //Inputs and Result
  public hexHeight: string = '0';
  public upperHeight: string = '0';
  public lowerHeight: string = '0';
  public upperRange: string = '1';
  public lowerRange: string = '1';
  public blocked: boolean = false;

  //Diagnostics and View Toggles
  public values: number[] = [];
  public showInfo: boolean = false;
  public showSnippetLOS: boolean = false;
  public showSnippetDeadZones: boolean = false;

  constructor() { }

  public doCalc() {
    let lowerHeight = parseInt(this.lowerHeight) + 1;
    let upperHeight = parseInt(this.upperHeight) + 1;
    let hexHeight = parseInt(this.hexHeight);
    let upperRange = parseInt(this.upperRange);
    let lowerRange = parseInt(this.lowerRange);

    let a = hexHeight - lowerHeight;
    let b = upperHeight - hexHeight;
    let c = (a - b) * 2;
    let d = upperRange - lowerRange;
    this.blocked = (d + c > 0) ? true : false;
    this.values = [a, b, c, d, d + c];
  }

}

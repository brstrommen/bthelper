import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';

import { ArtyService, LanceService, TurnService } from '../../providers';
import { DeadzonePage } from '../'

@Component({
  selector: 'page-utilities',
  templateUrl: 'utilities.html'
})
export class UtilitiesPage {

  constructor(
    public alertCtrl: AlertController,
    public nav: NavController,
    public artySvc: ArtyService,
    public lanceSvc: LanceService,
    public turnSvc: TurnService
  ) { }

  public gotoDeadzone() {
    this.nav.push(DeadzonePage);
  }

  public resetTurn() {
    this.alertCtrl.create({
      title: 'Reset Battle',
      message: 'Clear artillery fire, hidden units, and the turn counter?',
      buttons: [{
        text: 'Cancel', role: 'cancel'
      }, {
        text: 'Confirm', handler: () => {
          this.turnSvc.reset();
          this.artySvc.reset();
          this.lanceSvc.reset();
        }
      }]
    }).present();
  }

}

import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { ArtyService, TurnService } from '../../providers';


@Component({
  selector: 'page-classified',
  templateUrl: 'classified.html'
})
export class ClassifiedPage {

  constructor(
    public toastCtrl: ToastController,
    public artySvc: ArtyService,
    public turnSvc: TurnService
  ) { }

    public advanceTurn() {
      if (this.artySvc.pendingStrikes) {
        this.toastCtrl.create({
          message: 'There are Artillery Strikes that must be revealed this turn.',
          duration: 2250,
          position: 'middle'
        }).present();
      } else {
        this.turnSvc.advanceTurn();
      }
    }

}

import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { filter } from 'lodash';

import { LanceService } from '../../providers';
import { Unit } from '../../classes';


@Component({
  selector: 'page-hide-unit',
  templateUrl: 'hide-unit.html'
})
export class HideUnitPage {
  public units: Unit[];
  public hiddenUnit: Unit;
  public mapsheet: string;
  public hex: number;
  public facing: string;

  constructor(
    public viewCtrl: ViewController,
    public lanceSvc: LanceService
  ) {
    //
  }

  ionViewWillEnter() {
    this.units = filter(this.lanceSvc.myUnits, mech => mech.hidden === undefined);
  }

  public concealUnit() {
    this.hiddenUnit.hidden = true;
    this.hiddenUnit.location = `${this.mapsheet} @ ${this.hex} ➔ ${this.facing}`;
    this.lanceSvc.saveMechs(true);
    this.viewCtrl.dismiss();
  }

}

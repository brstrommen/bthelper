import { Component } from '@angular/core';
import { AlertController, ModalController, ToastController } from 'ionic-angular';
import { take } from 'rxjs/operators';

import { Unit } from '../../classes';
import { ArtyService, LanceService, TurnService } from '../../providers';
import { DeclarePage } from '../';

@Component({
  selector: 'page-combat',
  templateUrl: 'combat.html'
})
export class CombatPage {

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public artySvc: ArtyService,
    public lanceSvc: LanceService,
    public turnSvc: TurnService
  ) { }

  public declare(unit: Unit) {
    this.turnSvc.turn$.pipe(take(1)).subscribe(turn => {
      if (turn > 0) {
        this.modalCtrl.create(DeclarePage, { unit: unit }).present();
      } else {
        this.toastCtrl.create({
          message: `You can't declare fire on turn 0.`,
          duration: 2250,
          position: 'middle'
        }).present();
      }
    });
  }

  public viewDmgSummary() {
    this.alertCtrl.create({
      title: `Damage Summary`,
      message: `
        ${this.lanceSvc.opFor.map(unit => ({
          name: unit.getFullName(),
          damage: unit.damage
        })).map(unit => `${unit.name}: ${unit.damage}\n`)}
      `,
      buttons: [{
        text: 'Okay',
        role: 'cancel'
      }]
    }).present();
  }

  public advanceTurn() {
    if (this.artySvc.pendingStrikes) {
      this.toastCtrl.create({
        message: 'There are Artillery Strikes that must be revealed this turn.',
        duration: 2250,
        position: 'middle'
      }).present();
    } else {
      this.turnSvc.advanceTurn();
    }
  }

}

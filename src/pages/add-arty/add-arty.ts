import { Component } from '@angular/core';
import { ToastController, ViewController } from 'ionic-angular';
import { take } from 'rxjs/operators';
import { last } from 'lodash';

import { ArtyService, TurnService } from '../../providers';
import { Arty } from '../../classes';


@Component({
  selector: 'page-add-arty',
  templateUrl: 'add-arty.html'
})
export class AddArtyPage {
  public newStrike: Arty = new Arty();

  constructor(
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public artySvc: ArtyService,
    public turnSvc: TurnService
  ) {
    //
  }

  ionViewWillEnter() {
    this.turnSvc.turn$.pipe(take(1)).subscribe(currentTurn => {
      let lastStrike = last(this.artySvc.arty) || new Arty();
      this.newStrike.type = lastStrike.type;
      this.newStrike.mapsheet = lastStrike.mapsheet;
      this.newStrike.hex = lastStrike.hex;
      this.newStrike.turn = currentTurn + 1;
    });
  }

  public addArty() {
    this.turnSvc.turn$.pipe(take(1)).subscribe(currentTurn => {
      if (this.newStrike.turn <= currentTurn) {
        this.toastCtrl.create({
          duration: 2750,
          message: 'You cannot call an Artillery Strike for a turn that has already occurred.',
          position: 'middle'
        }).present();

        return;
      } else {
        this.artySvc.addArtyStrike(this.newStrike);
        this.viewCtrl.dismiss(true);
      }

    });
  }

}

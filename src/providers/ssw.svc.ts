import { Injectable } from '@angular/core';

import { find, forEach, map } from 'lodash';

import { Unit as Mech, Weapon } from '../classes';
import { WeaponService } from '../providers';
import { data } from '../assets/mech_data';


@Injectable()
export class SSWService {
  public mechs: Mech[] = [];

  constructor(
    public weaponSvc: WeaponService
  ) {
    console.log('Constructing SSWService');

    forEach(data, mech => {
      this.mechs.push(new Mech(
        mech.chassis,
        mech.variant,
        'NA', 4, 5, [],
        mech.description
      ))
    });
  }

  public getMechCopy(name: string): Mech {
    let mech = find(data, mech => `${mech.chassis} ${mech.variant}` === name)

    return new Mech(
      mech.chassis,
      mech.variant,
      'NA',
      4, 5,
      map(mech.weapons, newWep => {

        if (newWep.weapon.indexOf('(R)') !== -1) {
          newWep.weapon = newWep.weapon.substring(4);
          newWep.location = `${newWep.location} (R)`;
        }

        let foundWep = find(this.weaponSvc.weapons, weapon => weapon.name === newWep.weapon);
        if (foundWep) { foundWep = foundWep.getItem(); }
        else foundWep = new Weapon(`Unsupported Weapon: ${newWep.weapon}`, 0, [0, 0, 0, 0], 0);

        foundWep.location = newWep.location;
        return foundWep;
      }),
      mech.description
    );
  }

}

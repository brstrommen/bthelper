import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable()
export class StorageService {

  constructor (
    private storage: Storage
  ) { console.log('Constructing StorageService'); }

  public saveKey(key: string, value: any): Promise<any> {
    return this.storage.set(key, value);
  }

  public loadKey(key: string): Promise<any> {
    return this.storage.get(key);
  }

}

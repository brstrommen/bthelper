import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { take } from 'rxjs/operators';
import { filter, sortBy } from 'lodash';

import { StorageService, TurnService } from './';
import { Arty } from '../classes';


@Injectable()
export class ArtyService {
  public pendingStrikes: boolean = false;
  public arty: Arty[] = [];

  constructor (
    public toastCtrl: ToastController,
    public storageSvc: StorageService,
    public turnSvc: TurnService
  ) {
    console.log('Constructing ArtyService');

    this.storageSvc.loadKey('arty')
      .then(arty => {
        console.log('Successfully loaded Artillery from storage.')
        this.arty = arty || []
      }).catch(() => {
        console.log('Error loading Artillery from storage.')
        this.arty = [];
      });

    this.turnSvc.turn$.subscribe(() => this.checkPendingStrikes(true));
  }

  public saveArty(): Promise<any> {
    return this.storageSvc.saveKey('arty', this.arty);
  }

  public revealArty(strike: any): void {
    strike.revealed = true;
    this.checkPendingStrikes(true);
    this.saveArty();
  }

  public checkPendingStrikes(warn: boolean = false): void {
    this.turnSvc.turn$.pipe(take(1)).subscribe(currentTurn => {
      if(filter(this.arty, strike => !strike.revealed && (strike.turn == currentTurn)).length) {

        if (warn) {
          this.toastCtrl.create({
            message: 'There are Artillery Strikes that must be revealed this turn.',
            duration: 2250,
            position: 'middle'
          }).present();

          this.pendingStrikes = true;
        }
      } else {
        this.pendingStrikes = false;
      }
    })
  }

  public addArtyStrike(data: Arty): void {
    this.arty.push(data);
    this.arty = sortBy(this.arty, strike => strike.turn);
    this.saveArty();
  }

  public reset(): void {
    this.arty = [];
    this.checkPendingStrikes();
    this.saveArty();
  }

}

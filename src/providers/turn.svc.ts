import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

import { BehaviorSubject } from 'rxjs';

import { StorageService } from './';


@Injectable()
export class TurnService {
  private currentTurn: number = 0;
  public turn$: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor (
    public alertCtrl: AlertController,
    public storageSvc: StorageService
  ) {
    console.log('Constructing TurnService');

    this.storageSvc.loadKey('turn')
      .then(turn => {
        this.currentTurn = turn || 0
        this.turn$.next(this.currentTurn);
        console.log('Successfully loaded Turn Counter from storage.')
      }).catch(() => {
        this.currentTurn = 0;
        this.turn$.next(this.currentTurn);
        this.storageSvc.saveKey('turn', this.currentTurn);
        console.log('Error loading Turn Counter from storage.')
      });
  }

  public advanceTurn(): void {
    this.alertCtrl.create({
      title: 'Advance Turn Counter?',
      buttons: [{
        role: 'cancel',
        text: 'Cancel'
      }, {
        text: 'Advance',
        handler: () =>  {
          this.currentTurn++;
          this.turn$.next(this.currentTurn);
          this.storageSvc.saveKey('turn', this.currentTurn);
        }
      }]
    }).present();
  }

  public reset(): void {
    this.currentTurn = 0;
    this.turn$.next(this.currentTurn);
    this.storageSvc.saveKey('turn', this.currentTurn);
  }

}

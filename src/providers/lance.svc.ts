import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { forEach, pull } from 'lodash';

import { Unit } from '../classes';
import { SSWService, StorageService, TurnService } from './';


@Injectable()
export class LanceService {
  public myUnits: Unit[] = [];
  public opFor: Unit[] = [];

  constructor(
    public toastCtrl: ToastController,
    public sswSvc: SSWService,
    public storageSvc: StorageService,
    public turnSvc: TurnService
  ) {
    console.log('Constructing LanceService');
    this.turnSvc.turn$.subscribe(() => this.newTurn());
    this.loadMechs();
  }

  public saveMechs(self: boolean = true): void {
    let mechs = self ? this.myUnits : this.opFor;
    this.storageSvc.saveKey(self ? 'myMechs' : 'opForMechs', mechs.map(unit => ({
      chassis: unit.chassis,
      model: unit.model,
      description: unit.description,
      hidden: unit.hidden,
      location: unit.location,
      piloting: unit.piloting,
      gunnery: unit.gunnery
    })));
  }

  public loadMechs(): void {
    Promise.all([
      this.storageSvc.loadKey('myMechs'),
      this.storageSvc.loadKey('opForMechs')
    ]).then(([myUnits, opFor]) => {
      forEach(myUnits, unit => this.myUnits.push(
        this.configureMech(unit)
      ));

      forEach(opFor, unit => this.opFor.push(
        this.configureMech(unit)
      ));
    });
  }

  private configureMech(unit) {
    let newMech = this.sswSvc.getMechCopy(`${unit.chassis} ${unit.model}`);
    newMech.piloting = unit.piloting;
    newMech.gunnery = unit.gunnery;
    newMech.description = unit.description;
    newMech.hidden = unit.hidden;
    newMech.location = unit.location;

    return newMech;
  }

  public addMyUnit(unit: Unit): void {
    this.myUnits.push(unit);
    this.saveMechs(true);
  }

  public addOpForUnit(unit: Unit): void {
    this.opFor.push(unit);
    this.saveMechs(false);
  }

  public deleteMyUnit(unit: Unit): void {
    pull(this.myUnits, unit);

    this.toastCtrl.create({
      message: `Removed ${unit.getFullName()} from your Lance.`,
      duration: 1650,
      position: 'middle'
    }).present();

    this.saveMechs(true);
  }

  public deleteOpForUnit(unit): void {
    pull(this.opFor, unit);

    this.toastCtrl.create({
      message: `Removed ${unit.getFullName()} from the OpFor.`,
      duration: 1650,
      position: 'middle'
    }).present();

    this.saveMechs(false);
  }

  public newTurn(): void {
    this.myUnits.forEach(unit => unit.newTurn());
    this.opFor.forEach(unit => unit.newTurn());
  }

  public reset(): void {
    this.myUnits.forEach(unit => unit.hidden = undefined);
    this.opFor.forEach(unit => unit.hidden = undefined);
  }

}

import { sum } from 'lodash';

import { Unit } from './';

const RANGES = {
  MIN: 0,
  SHORT: 1,
  MED: 2,
  LONG: 3
};

const PROBABILITY = {
  2: 100,
  3: 97.22,
  4: 91.67,
  5: 83.33,
  6: 72.22,
  7: 58.33,
  8: 41.67,
  9: 27.78,
  10: 16.67,
  11: 8.33,
  12: 2.78
};


export class Weapon {
  public name: string;
  public heat: number;
  public range: number[];
  public location: string;
  public damage: number;

  private baseAccuracy: number;

  //Turn Properties
  public target: Unit;
  public probability: number;
  public toHitMods: number[];
  public toHit: number;
  public attackStatus: string;

  constructor(
    name: string,
    heat: number,
    ranges: number[],
    damage: number,
    accuracy: number = 0
  ) {
    this.name = name;
    this.heat = heat;
    this.range = ranges;
    this.toHitMods = [];
    this.location = 'UNK';
    this.damage = damage;
    this.baseAccuracy = accuracy;

    if (name.indexOf('Unsupported Weapon') !== -1) { console.log(name); }
    this.newTurn();
  }

  public calcToHit(
    gunnery: number,
    moveType: number,
    moveMod: number,
    other: number,
    range: number
  ): number {
    other += this.baseAccuracy;
    let toHit = 0;

    let rangeMod =
      range <= this.range[RANGES.MIN]
      ? (this.range[RANGES.MIN] - range) + 1
      : range <= this.range[RANGES.SHORT] ? 0
        : range <= this.range[RANGES.MED] ? 2
        : range <= this.range[RANGES.LONG] ? 4
        : NaN;

    this.toHitMods = [
      gunnery || 0,
      moveType || 0,
      moveMod || 0,
      other || 0,
      rangeMod
    ];

    toHit = sum(this.toHitMods);
    return toHit <= 12 ? toHit : NaN;
  }

  public newTurn() {
    this.target = null;
    this.toHit = NaN;
    this.toHitMods = [];
    this.toHitMods.fill(0, 5);
    this.attackStatus = 'none';
  }

  public getProbability() {
    return PROBABILITY[this.toHit];
  }

  public resolveAttack(status: string) {
    this.attackStatus = status;
    if (status === 'hit') {
      this.target.damage += this.damage;
    }
  }

}

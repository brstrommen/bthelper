import { Weapon } from './';

export class CapacitorWeapon extends Weapon {
  public firingModes: string[] = [
    'Charge Capacitor',
    'Fire Weapon',
    'Fire w/ Capacitor'
  ];

  public setFiringMode(mode: string) {
    switch(mode) {
      case 'Charge Capacitor': {
        this.heat = 5;
        break;
      } case 'Fire Weapon': {
        this.heat = 10;
        break;
      } case 'Fire w/ Capacitor': {
        this.heat = 15;
        break;
      }
    }
  }

}

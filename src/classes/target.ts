import { Unit, Weapon } from './';


export class Target {
  unit: Unit;
  cover: number;
  range: number;
  weapons: Weapon[];

  constructor() {
    this.cover = 0;
    this.range = 1;
    this.unit = null;
  }

}

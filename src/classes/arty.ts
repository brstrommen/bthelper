export class Arty {
  public type: string;
  public mapsheet: string;
  public hex: string;
  public turn: number;
  public revealed: boolean;

  constructor() { }

}

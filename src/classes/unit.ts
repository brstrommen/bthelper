import { sum } from 'lodash';

import { Target, Weapon } from './';


export class Unit {
  //Descriptors
  description: string = null;
  chassis: string;
  model: string;
  summary: string;
  hidden: boolean;
  location: string;

  //Stats
  gunnery: number;
  piloting: number;

  //Arms
  weapons: Weapon[] = [];

  //Turn Properties
  turnHeat: number; //Self only
  moveType: number; //Self only
  jumping: number; //Self only
  targets: Target[]; //Self only
  moveMod: number; //Enemies only
  damage: number;

  constructor(
    chassis: string = '',
    model: string = '',
    description: string = 'NA',
    gunnery: number = 4,
    piloting: number = 5,
    weapons: Weapon[] = [],
    summary: string = 'NA'
  ) {
    this.chassis = chassis;
    this.model = model;
    this.description = description;
    this.gunnery = gunnery;
    this.piloting = piloting;
    this.summary = summary;
    weapons.forEach(weapon => this.weapons.push(weapon));
    this.newTurn();
  }

  public getShortName(): string {
    return `${this.chassis} ${this.model}`;
  }

  public getFullName(): string {
    let description = this.description !== "NA"
      ? ` (${this.description})`
      : "";

    return `${this.chassis} ${this.model}${description}`;
  }

  public getMoveMode(): string {
    switch(this.moveType) {
      case 0: return 'Stood Still';
      case 1: return 'Walk';
      case 2: return 'Run';
      case 3: return 'Jump';
    }
  }

  public newTurn() {
    this.turnHeat = 0;
    this.moveMod = 0;
    this.moveType = 0;
    this.jumping = 0;
    this.damage = 0;
    this.targets = [ new Target() ];
    this.weapons.forEach(weapon => weapon.newTurn());
  }

  public calculateTurnHeat() {
    let weaponHeat = sum(this.weapons
      .filter(weapon => weapon.target !== null)
      .map(weapon => weapon.heat)
    );

    let movementHeat = this.moveType !== 3 ? this.moveType : Math.max(this.moveType, this.jumping);

    this.turnHeat = weaponHeat + movementHeat;
  }

}

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';

import {
  AddPage,
  AddArtyPage,
  CombatPage,
  HomePage,
  TabsPage,
  DeclarePage,
  DeadzonePage,
  UtilitiesPage,
  ClassifiedPage,
  HideUnitPage
} from '../pages';

import {
  WeaponService,
  SSWService,
  StorageService,
  TurnService,
  LanceService,
  ArtyService
} from '../providers';

import {
  AttackComponent,
  AttackInfoComponent,
  LanceContainerComponent,
  ArtyContainerComponent,
  ConcealedContainerComponent
} from '../components';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@NgModule({
  declarations: [
    MyApp,
    AddPage,
    AddArtyPage,
    CombatPage,
    HomePage,
    TabsPage,
    DeclarePage,
    DeadzonePage,
    ClassifiedPage,
    UtilitiesPage,
    HideUnitPage,
    AttackComponent,
    AttackInfoComponent,
    LanceContainerComponent,
    ArtyContainerComponent,
    ConcealedContainerComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddPage,
    AddArtyPage,
    CombatPage,
    HomePage,
    TabsPage,
    DeclarePage,
    DeadzonePage,
    UtilitiesPage,
    ClassifiedPage,
    HideUnitPage,
    AttackComponent,
    AttackInfoComponent,
    LanceContainerComponent,
    ArtyContainerComponent,
    ConcealedContainerComponent
  ],
  providers: [
    WeaponService,
    SSWService,
    StorageService,
    TurnService,
    LanceService,
    ArtyService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
